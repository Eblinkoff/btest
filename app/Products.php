<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $fillable = ['article', 'name', 'status', 'data'];

	public $timestamps = false;

	// public static $table = 'products';
	
	/**
	* Local Scope для получения только доступных продуктов (STATUS = “available”).
	*
	* @param \Illuminate\Database\Eloquent\Builder $query
	* @return \Illuminate\Database\Eloquent\Builder
	*/
	public function scopeAvailable_only($query)
	{
		return $query->where('status', 'LIKE', 'available');
	}
	
	public function test($request_all)
	{
		return $request_all;
	}	
}
