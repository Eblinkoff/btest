<?php

namespace App\Http\Controllers\Api\V1;

use App\Products;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Products::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
		return 'create';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$this->validate($request, [
			'name' => 'required|min:10',
			'article' => 'required|alpha_num|unique:products,article',
		]);
		// надо ли говорить, что для боевого проекта такой валидации недостаточно, но это тестовое задание и я делаю только то, что в нём есть
		$product = new Products();
		$product->name = $request->name;
		$product->article = $request->article;
		$product->status = $request->status;
		// jsonb
		if($request->data != null)
		{
			$json = '';
			for($i = 0; $i < count($request->data['key']);$i++)
			{
				if(isset($request->data['key'][$i]) && $request->data['key'][$i] != '')
				{
					$json .= ($i > 0 ? ',' : '').'"'.$request->data['key'][$i].'": "'.$request->data['value'][$i].'"';
				}
			}
			// '{"a": 1}'
			$product->data = '{'.$json.'}';
		}
		else
		{
			$product->data = '{}';
		}
		$product->save();
		$returnHTML = view('products_list')->with('products', Products::all())->render();
		return response()->json(array('success' => true, 'table'=>$returnHTML));
	}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		$returnHTML = view('products_cart')->with('product', Products::findOrFail($id))->render();
		return response()->json(array('success' => true, 'cart'=>$returnHTML));
	}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
		$returnHTML = view('products_cart_edit')->with('product', Products::findOrFail($id))->render();
		return response()->json(array('success' => true, 'cart'=>$returnHTML));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$this->validate($request, [
			'name' => 'required|min:10',
			'article' => 'required|alpha_num',
		]);
		// надо ли говорить, что для боевого проекта такой валидации недостаточно, но это тестовое задание и я делаю только то, что в нём есть
		$product = Products::find($id);
		// $product = Products::findOrFail($id);
		$product->name = $request->name;
		$product->article = $request->article;
		$product->status = $request->status;
		// jsonb
		if($request->data != null)
		{
			$json = '';
			for($i = 0; $i < count($request->data['key']);$i++)
			{
				if(isset($request->data['key'][$i]) && $request->data['key'][$i] != '')
				{
					$json .= ($i > 0 ? ',' : '').'"'.$request->data['key'][$i].'": "'.$request->data['value'][$i].'"';
				}
			}
			// '{"a": 1}'
			$product->data = '{'.$json.'}';
		}
		else
		{
			$product->data = '{}';
		}
		$product->save();
		$returnHTML = view('products_list')->with('products', Products::all())->render();
		return response()->json(array('success' => true, 'table'=>$returnHTML));
	}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Products::findOrFail($id);
        $product->delete();
		$returnHTML = view('products_list')->with('products', Products::all())->render();
		return response()->json(array('success' => true, 'table'=>$returnHTML));
	}
}
