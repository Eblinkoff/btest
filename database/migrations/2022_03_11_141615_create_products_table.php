<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('article')->unique();
            $table->char('name');
            $table->char('status');
            $table->jsonb('data');
            $table->timestamps();
            $table->integer('soft_deletes')->default(0);// 0 - не удалено, 1 - удалено
			// индекс
			$table->index('article');
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
