<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
config(['products.role' => '1']);// id администратора
config(['products.email' => 'eblinkoff@mail.ru']);// email администратора
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/products', 'ProductController@index')->name('products');// страница со списком продуктов

/*
GET 	/photos 	index 	photo.index
GET 	/photos/create 	create 	photo.create
POST 	/photos 	store 	photo.store
GET 	/photos/{photo} 	show 	photo.show
GET 	/photos/{photo}/edit edit 	photo.edit
PUT/PATCH/photos/{photo} 	update 	photo.update
DELETE 	/photos/{photo} 	destroy 	photo.destroy
*/

Route::post('/products', 'ProductController@store');// получение подвьюхи со списком продуктов

Route::get('/products/{id}', 'ProductController@show');// получение карточки продукта

Route::get('/products/{id}/edit', 'ProductController@edit');// получение формы редактирования продукта

Route::post('/products/{id}/update', 'ProductController@update');// редактирование продукта

Route::get('/products/{id}/delete', 'ProductController@destroy');// удаление продукта
