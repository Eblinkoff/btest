<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
config(['products.role' => '1']);// id администратора
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::group(['prefix' => '/v1', 'namespace' => 'Api\V1', 'as' => 'api.'], function () {
    // Route::resource('products', 'ProductsController');
// });
// , ['except' => ['create', 'edit']]
/**
GET 	/photos 	index 	photo.index
GET 	/photos/create 	create 	photo.create
POST 	/photos 	store 	photo.store
GET 	/photos/{photo} 	show 	photo.show
GET 	/photos/{photo}/edit edit 	photo.edit
PUT/PATCH/photos/{photo} 	update 	photo.update
DELETE 	/photos/{photo} 	destroy 	photo.destroy


*/