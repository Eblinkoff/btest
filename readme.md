## Учебный проект на Laravel

## Условия были такие

<div class=Section1>

<p class=normal><span style='font-size:12.0pt;line-height:115%'>Разработать функционал
на <b style='mso-bidi-font-weight:normal'>Laravel</b> c базой данных <b
style='mso-bidi-font-weight:normal'>PostgreSQL</b>.<o:p></o:p></span></p>

<p class=normal><span style='font-size:12.0pt;line-height:115%'><br>
Реализовать вывод списка продуктов, просмотр карточки продукта, добавление,<span
style='mso-spacerun:yes'>  </span>редактирование и удаление продукта.<o:p></o:p></span></p>

<p class=normal><span style='font-size:12.0pt;line-height:115%'><o:p>&nbsp;</o:p></span></p>

<p class=normal><b style='mso-bidi-font-weight:normal'><span style='font-size:
12.0pt;line-height:115%'>Создать таблицу</span></b><span style='font-size:12.0pt;
line-height:115%'> «products».<o:p></o:p></span></p>

<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=601
 style='width:450.75pt;margin-left:-4.9pt;border-collapse:collapse;border:none;
 mso-border-alt:solid black 1.0pt;mso-padding-alt:5.0pt 5.0pt 5.0pt 5.0pt;
 mso-border-insideh:1.0pt solid black;mso-border-insidev:1.0pt solid black'>
 <thead>
  <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;page-break-inside:avoid'>
   <td width=89 valign=top style='width:66.75pt;border:solid black 1.0pt;
   padding:5.0pt 5.0pt 5.0pt 5.0pt'>
   <p class=normal style='line-height:normal;mso-pagination:none'>ID</p>
   </td>
   <td width=214 valign=top style='width:160.5pt;border:solid black 1.0pt;
   border-left:none;mso-border-left-alt:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
   <p class=normal style='line-height:normal;mso-pagination:none'>uint,
   autoincrement</p>
   </td>
   <td width=298 valign=top style='width:223.5pt;border:solid black 1.0pt;
   border-left:none;mso-border-left-alt:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
   <p class=normal style='line-height:normal;mso-pagination:none'><o:p>&nbsp;</o:p></p>
   </td>
  </tr>
  <tr style='mso-yfti-irow:1;page-break-inside:avoid'>
   <td width=89 valign=top style='width:66.75pt;border:solid black 1.0pt;
   border-top:none;mso-border-top-alt:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
   <p class=normal style='line-height:normal;mso-pagination:none'>ARTICLE</p>
   <p class=normal style='line-height:normal;mso-pagination:none'><o:p>&nbsp;</o:p></p>
   </td>
   <td width=214 valign=top style='width:160.5pt;border-top:none;border-left:
   none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
   mso-border-top-alt:solid black 1.0pt;mso-border-left-alt:solid black 1.0pt;
   padding:5.0pt 5.0pt 5.0pt 5.0pt'>
   <p class=normal style='line-height:normal;mso-pagination:none'>varchar(255),
   unique index</p>
   </td>
   <td width=298 valign=top style='width:223.5pt;border-top:none;border-left:
   none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
   mso-border-top-alt:solid black 1.0pt;mso-border-left-alt:solid black 1.0pt;
   padding:5.0pt 5.0pt 5.0pt 5.0pt'>
   <p class=normal style='line-height:normal;mso-pagination:none'><o:p>&nbsp;</o:p></p>
   </td>
  </tr>
  <tr style='mso-yfti-irow:2;page-break-inside:avoid'>
   <td width=89 valign=top style='width:66.75pt;border:solid black 1.0pt;
   border-top:none;mso-border-top-alt:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
   <p class=normal style='line-height:normal;mso-pagination:none'>NAME</p>
   </td>
   <td width=214 valign=top style='width:160.5pt;border-top:none;border-left:
   none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
   mso-border-top-alt:solid black 1.0pt;mso-border-left-alt:solid black 1.0pt;
   padding:5.0pt 5.0pt 5.0pt 5.0pt'>
   <p class=normal style='line-height:normal;mso-pagination:none'>varchar(255)</p>
   </td>
   <td width=298 valign=top style='width:223.5pt;border-top:none;border-left:
   none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
   mso-border-top-alt:solid black 1.0pt;mso-border-left-alt:solid black 1.0pt;
   padding:5.0pt 5.0pt 5.0pt 5.0pt'>
   <p class=normal style='line-height:normal;mso-pagination:none'><o:p>&nbsp;</o:p></p>
   </td>
  </tr>
  <tr style='mso-yfti-irow:3;page-break-inside:avoid'>
   <td width=89 valign=top style='width:66.75pt;border:solid black 1.0pt;
   border-top:none;mso-border-top-alt:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
   <p class=normal style='line-height:normal;mso-pagination:none'>STATUS</p>
   </td>
   <td width=214 valign=top style='width:160.5pt;border-top:none;border-left:
   none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
   mso-border-top-alt:solid black 1.0pt;mso-border-left-alt:solid black 1.0pt;
   padding:5.0pt 5.0pt 5.0pt 5.0pt'>
   <p class=normal style='line-height:normal;mso-pagination:none'>varchar(255)</p>
   </td>
   <td width=298 valign=top style='width:223.5pt;border-top:none;border-left:
   none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
   mso-border-top-alt:solid black 1.0pt;mso-border-left-alt:solid black 1.0pt;
   padding:5.0pt 5.0pt 5.0pt 5.0pt'>
   <p class=normal style='line-height:normal;mso-pagination:none'>&quot;available&quot;
   | &quot;unavailable&quot;</p>
   </td>
  </tr>
  <tr style='mso-yfti-irow:4;page-break-inside:avoid'>
   <td width=89 valign=top style='width:66.75pt;border:solid black 1.0pt;
   border-top:none;mso-border-top-alt:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
   <p class=normal style='line-height:normal;mso-pagination:none'>DATA</p>
   </td>
   <td width=214 valign=top style='width:160.5pt;border-top:none;border-left:
   none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
   mso-border-top-alt:solid black 1.0pt;mso-border-left-alt:solid black 1.0pt;
   padding:5.0pt 5.0pt 5.0pt 5.0pt'>
   <p class=normal style='line-height:normal;mso-pagination:none'>jsonb</p>
   </td>
   <td width=298 valign=top style='width:223.5pt;border-top:none;border-left:
   none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
   mso-border-top-alt:solid black 1.0pt;mso-border-left-alt:solid black 1.0pt;
   padding:5.0pt 5.0pt 5.0pt 5.0pt'>
   <p class=normal style='line-height:normal;mso-pagination:none'>несколько разных
   полеи&#774; <br>
   (например, Color и Size) <br>
   <i style='mso-bidi-font-style:normal'>на своё усмотрение<o:p></o:p></i></p>
   </td>
  </tr>
  <tr style='mso-yfti-irow:5;page-break-inside:avoid'>
   <td width=89 valign=top style='width:66.75pt;border:solid black 1.0pt;
   border-top:none;mso-border-top-alt:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
   <p class=normal style='line-height:normal;mso-pagination:none'><o:p>&nbsp;</o:p></p>
   </td>
   <td width=214 valign=top style='width:160.5pt;border-top:none;border-left:
   none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
   mso-border-top-alt:solid black 1.0pt;mso-border-left-alt:solid black 1.0pt;
   padding:5.0pt 5.0pt 5.0pt 5.0pt'>
   <p class=normal style='line-height:normal;mso-pagination:none'>timestamps</p>
   </td>
   <td width=298 valign=top style='width:223.5pt;border-top:none;border-left:
   none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
   mso-border-top-alt:solid black 1.0pt;mso-border-left-alt:solid black 1.0pt;
   padding:5.0pt 5.0pt 5.0pt 5.0pt'>
   <p class=normal style='line-height:normal;mso-pagination:none'><o:p>&nbsp;</o:p></p>
   </td>
  </tr>
  <tr style='mso-yfti-irow:6;mso-yfti-lastrow:yes;page-break-inside:avoid'>
   <td width=89 valign=top style='width:66.75pt;border:solid black 1.0pt;
   border-top:none;mso-border-top-alt:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
   <p class=normal style='line-height:normal;mso-pagination:none'><o:p>&nbsp;</o:p></p>
   </td>
   <td width=214 valign=top style='width:160.5pt;border-top:none;border-left:
   none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
   mso-border-top-alt:solid black 1.0pt;mso-border-left-alt:solid black 1.0pt;
   padding:5.0pt 5.0pt 5.0pt 5.0pt'>
   <p class=normal style='line-height:normal;mso-pagination:none'>soft deletes</p>
   </td>
   <td width=298 valign=top style='width:223.5pt;border-top:none;border-left:
   none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
   mso-border-top-alt:solid black 1.0pt;mso-border-left-alt:solid black 1.0pt;
   padding:5.0pt 5.0pt 5.0pt 5.0pt'>
   <p class=normal style='line-height:normal;mso-pagination:none'><o:p>&nbsp;</o:p></p>
   </td>
  </tr>
 </thead>
</table>

<p class=normal><span style='font-size:12.0pt;line-height:115%'><o:p>&nbsp;</o:p></span></p>

<p class=normal><b style='mso-bidi-font-weight:normal'><span style='font-size:
12.0pt;line-height:115%'>Создать Eloquent-модель</span></b><span
style='font-size:12.0pt;line-height:115%'> «Product», связанную с таблицеи&#774;
«products».<o:p></o:p></span></p>

<p class=normal><span style='font-size:12.0pt;line-height:115%'>В модели реализовать
<b style='mso-bidi-font-weight:normal'>Local Scope</b> для получения только доступных
продуктов (STATUS = “available”).<o:p></o:p></span></p>

<p class=normal><span style='font-size:12.0pt;line-height:115%'><o:p>&nbsp;</o:p></span></p>

<p class=normal><b style='mso-bidi-font-weight:normal'><span style='font-size:
12.0pt;line-height:115%'>Сделать валидацию</span></b><span style='font-size:
12.0pt;line-height:115%'> создания и редактирования:<o:p></o:p></span></p>

<p class=normal><span style='font-size:12.0pt;line-height:115%'>NAME — обязательное
поле, длинои&#774; не менее 10 символов;<o:p></o:p></span></p>

<p class=normal><span style='font-size:12.0pt;line-height:115%'>ARTICLE — обязательное
поле, только латинские символы и цифры, уникальное в таблице.<o:p></o:p></span></p>

<p class=normal><span style='font-size:12.0pt;line-height:115%'><o:p>&nbsp;</o:p></span></p>

<p class=normal><b style='mso-bidi-font-weight:normal'><span style='font-size:
12.0pt;line-height:115%'>Создать роль администратора</span></b><span
style='font-size:12.0pt;line-height:115%'>, который может редактировать артикул,
остальным пользователям редактирование артикула недоступно.<o:p></o:p></span></p>

<p class=normal><span style='font-size:12.0pt;line-height:115%'>Роль пользователя
можно узнать из настроек (config(‘products.role’)).<o:p></o:p></span></p>

<p class=normal><b style='mso-bidi-font-weight:normal'><span style='font-size:
12.0pt;line-height:115%'>Реализовать валидацию и проверку прав</span></b><span
style='font-size:12.0pt;line-height:115%'> (контроллер, модель, отдельныи&#774;
сервис — на своё усмотрение).<o:p></o:p></span></p>

<p class=normal><span style='font-size:12.0pt;line-height:115%'><o:p>&nbsp;</o:p></span></p>

<p class=normal><u><span style='font-size:12.0pt;line-height:115%'>Дополнительно</span></u><span
style='font-size:12.0pt;line-height:115%'>:<o:p></o:p></span></p>

<p class=normal><span style='font-size:12.0pt;line-height:115%'>При создании продукта
<b style='mso-bidi-font-weight:normal'>реализовать отправку на заданныи&#774; в
конфигурации Email</b> (config(‘products.email’)) <b style='mso-bidi-font-weight:
normal'>уведомления</b> (Notification) о том, что продукт создан.<o:p></o:p></span></p>

<p class=normal><span style='font-size:12.0pt;line-height:115%'>Уведомление должно
отправляться через задачу (Job) в очереди (Queue).<o:p></o:p></span></p>

<p class=normal><span style='font-size:12.0pt;line-height:115%'>Готовое приложение
упаковать в docker.<o:p></o:p></span></p>

<p class=normal><span style='font-size:12.0pt;line-height:115%'><o:p>&nbsp;</o:p></span></p>

<p class=normal><b style='mso-bidi-font-weight:normal'><span style='font-size:
12.0pt;line-height:115%'>Интерфейс приложения реализовать соответственно макету</span></b><span
style='font-size:12.0pt;line-height:115%'> (см. ссылку): </span><a
href="https://www.figma.com/proto/hQ0IZkC4ULeX7vwiEXN2q3/PIN-ERP-%D0%A2%D0%97-03.02.2022?node-id=3%3A218&amp;scaling=min-zoom&amp;page-id=0%3A1&amp;starting-point-node-id=3%3A218"><span
style='font-size:12.0pt;line-height:115%;color:#1155CC'>https://www.figma.com/proto/hQ0IZkC4ULeX7vwiEXN2q3/PIN-ERP-%D0%A2%D0%97-03.02.2022?node-id=3%3A218&amp;scaling=min-zoom&amp;page-id=0%3A1&amp;starting-point-node-id=3%3A218</span></a><span
style='font-size:12.0pt;line-height:115%'><o:p></o:p></span></p>

<p class=normal><span style='font-size:12.0pt;line-height:115%'><o:p>&nbsp;</o:p></span></p>

<p class=normal><b style='mso-bidi-font-weight:normal'><span style='font-size:
12.0pt;line-height:115%'>Готовое приложение выложить на GitHub / Bitbucket</span></b><span
style='font-size:12.0pt;line-height:115%'><o:p></o:p></span></p>

</div>
