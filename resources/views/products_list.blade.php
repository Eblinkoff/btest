<div>
	@if($products != null)
	<table class="table my-table">
	  <thead>
		<tr>
		  <th>АРТИКУЛ</th>
		  <th>НАЗВАНИЕ</th>
		  <th>СТАТУС</th>
		  <th>АТРИБУТЫ</th>
		</tr>
	  </thead>
	  <tbody>
		@foreach($products as $product)
		<tr style="background-color: #fff;" onclick="showProductCart({{ $product->id }})">
		  <th scope="row">{{ $product->article }}</th>
		  <td>{{ $product->name }}</td>
		  <td>{{ $product->status }}</td>
		  @php
		  $json = json_decode($product->data);
		  if(!(array)$json)
		  {
			 $data = '';
		  }
		  else
		  {
			  $data = '';
			  foreach($json as $key => $attr)
			  {
				  $data .= $key.': '.$attr.'<br>';
			  }
		  }
		  @endphp
		  <td>
		  @php
		  echo $data;
		  @endphp
		  </td>
		</tr>
		@endforeach
	  </tbody>
	</table>
	@else
		Ни одного товара нет
	@endif
</div>
