@extends('layouts.plural')

@section('content')
<div class="left-sidebar nav-left">
	<div class="notice">
	{{ $title }}
	</div>
</div>
<div class="half-to-half">
	<div class="col-md-6" id="one_half_product">
		@include('products_list')
	</div>
	<div class="col-md-6" id="two_half_product">
		<button class="btn btn-info add-my-button" type="button" onclick="addProduct()">Добавить</button>
		<div class="add-product-form" style="display:none;">
			<span class="delete" onclick="deleteAddProduct()">X</span>
			<form class="new-product-form">
				{{ csrf_field() }}
				<div class="h3 my-form-header">Добавить продукт</div>
				<div class="alert alert-danger" role="alert" style="display:none;">
				</div>
				<div class="form-group">
					<label for="article">Артикул</label>
					<input required class="form-control" id="article" name="article" aria-describedby="articleHelp" placeholder="Артикул" type="text">
					<small id="articleHelp" class="form-text text-muted">* Обязательное поле</small>
				</div>
				<div class="form-group">
					<label for="article">Название</label>
					<input required class="form-control" name="name" id="name" aria-describedby="nameHelp" placeholder="Название" type="text">
					<small id="nameHelp" class="form-text text-muted">* Обязательное поле</small>
				</div>
				<div class="form-group">
					<label for="status">Статус</label>
					<select class="form-control" id="status" name="status">
						<option value="Доступен">Доступен</option>
						<option value="Недоступен">Недоступен</option>
					</select>
				</div>				
				<div class="h5 my-form-header">Атрибуты</div>
				<span class="pseudo-link" onclick="addAtributForm(this)">+ добавить атрибут</span>
				<br>
				<div class="atributes-list"></div>
				<button class="btn btn-info add-my-button" style="float:none;" type="button" onclick="return createNewProduct()">Добавить</button>
			</form>
		</div>
		<div class="add-product-form-cart" style="display:none;">
		</div>
	</div>
</div>
@endsection
