<span class="delete" onclick="deleteCartUniversal(this)">X</span>
<span class="edit-icon-wrapper" onclick="showEditProductForm({{ $product->id }})">
	<i class="bi-pencil"></i>
</span>
<span class="trash-icon-wrapper" onclick="deleteProduct({{ $product->id }})">
	<i class="bi-trash"></i>
</span>
<form class="new-product-form">
	<div class="h3 my-form-header">{{ $product->name }}</div>
	<div class="alert alert-danger" role="alert" style="display:none;">
	</div>
	<table class="table my-show-table">
	  <tbody>
		<tr>
		  <th scope="row">Артикул</th>
		  <td>{{ $product->article }}</td>
		  </td>
		</tr>
		<tr>
		  <th scope="row">Название</th>
		  <td>{{ $product->name }}</td>
		  </td>
		</tr>
		<tr>
		  <th scope="row">Статус</th>
		  <td>{{ $product->status }}</td>
		  </td>
		</tr>
		  @php
		  $json = json_decode($product->data);
		  if(!(array)$json)
		  {
			 $data = '';
		  }
		  else
		  {
			  $data = '';
			  foreach($json as $key => $attr)
			  {
				  $data .= $key.': '.$attr.'<br>';
			  }
		  }
		  if($data != '')
		  {
			  echo '
			<tr>
			  <th scope="row">Атрибуты</th>
			  <td>'.$data.'</td>
			  </td>
			</tr>
			';
		  }
		  @endphp
	  </tbody>
	</table>
</form>

