<span class="delete" onclick="deleteCartUniversal(this)">X</span>
<form class="new-product-form">
	{{ csrf_field() }}
	<div class="h3 my-form-header">Редактировать продукт</div>
	<div class="alert alert-danger" role="alert" style="display:none;">
	</div>
	@php
	if(Auth::check() && Auth::user()->id == (int) config('products.role'))
	{
	@endphp
	<div class="form-group">
		<label for="article">Артикул</label>
		<input required class="form-control" name="article" aria-describedby="articleHelp1" placeholder="Артикул" type="text" value="{{ $product->article }}">
		<small id="articleHelp1" class="form-text text-muted">* Обязательное поле</small>
	</div>
	@php
	}
	else
	{
	@endphp
	<table class="table my-show-table">
		<tr>
		  <th scope="row">Артикул</th>
		  <td>{{ $product->article }}</td>
		  </td>
		</tr>
	</table>
	@php
	}
	@endphp
	<div class="form-group">
		<label for="article">Название</label>
		<input required class="form-control" name="name" aria-describedby="nameHelp1" placeholder="Название" type="text" value="{{ $product->name }}">
		<small id="nameHelp1" class="form-text text-muted">* Обязательное поле</small>
	</div>
	<div class="form-group">
		<label for="status">Статус</label>
		<select class="form-control" name="status">
			<option value="Доступен"@php if(trim($product->status) == 'Доступен') echo ' selected'; @endphp>Доступен</option>
			<option value="Недоступен"@php if(trim($product->status) == 'Недоступен') echo ' selected'; @endphp>Недоступен</option>
		</select>
	</div>				
	<div class="h5 my-form-header">Атрибуты</div>
	<span class="pseudo-link" onclick="addAtributForm(this)">+ добавить атрибут</span>
	<br>
	<div class="atributes-list">
		  @php
		  $json = json_decode($product->data);
		  if(!(array)$json)
		  {
			 $data = '';
		  }
		  else
		  {
			  $data = '';
			  foreach($json as $key => $attr)
			  {
				  $data .= '
				  <div class="one-atrtr form-group">
					<input placeholder="Ключ" name="data[key][]" type="text" value="'.$key.'">
					<input placeholder="Значение" name="data[value][]" type="text" value="'.$attr.'">
					<span class="delete-attr" onclick="deleteattribute(this)">X</span>
				  </div>
				  ';
			  }
		  }
		  if($data != '')
		  {
			  echo $data;
		  }
		  @endphp
	</div>
	<button class="btn btn-info add-my-button" style="float:none;" type="button" onclick="return createNewEditProduct({{ $product->id }})">Добавить</button>
</form>
