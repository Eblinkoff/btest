@extends('layouts.app')

@section('content')
<div class="left-sidebar nav-left">
	<div class="notice">
		@yield('title')
	</div>
</div>
<div class="col-md-8 col-md-offset-2">
	<div class="panel panel-default">
		<div class="panel-heading">Dashboard</div>

		<div class="panel-body">
			@if (session('status'))
				<div class="alert alert-success">
					{{ session('status') }}
				</div>
			@endif

			You are logged in!!!!
			
			User_id = @php echo Auth::user()->id;

			@endphp
		</div>
	</div>
</div>
@endsection
